'use strict';
module.exports = (sequelize, DataTypes) => {
  const Feedback = sequelize.define('Feedback', {
    headline: DataTypes.STRING,
    description: DataTypes.TEXT,
    action: DataTypes.STRING
  }, {});
  Feedback.associate = function(models) {
    // associations can be defined here
  };
  return Feedback;
};