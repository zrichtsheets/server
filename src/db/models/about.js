'use strict';
module.exports = (sequelize, DataTypes) => {
  const About = sequelize.define('About', {
    technology: DataTypes.STRING,
    type: DataTypes.STRING,
    description: DataTypes.TEXT,
    link: DataTypes.STRING
  }, {});
  About.associate = function(models) {
    // associations can be defined here
  };
  return About;
};