'use strict';
module.exports = (sequelize, DataTypes) => {
  const Experience = sequelize.define('Experience', {
    company: DataTypes.STRING,
    start: DataTypes.STRING,
    end: DataTypes.STRING,
    role: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {});
  Experience.associate = function(models) {
    // associations can be defined here
  };
  return Experience;
};