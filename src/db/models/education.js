'use strict';
module.exports = (sequelize, DataTypes) => {
  const Education = sequelize.define('Education', {
    institution: DataTypes.STRING,
    start: DataTypes.STRING,
    end: DataTypes.STRING,
    major: DataTypes.STRING,
    accomplishments: DataTypes.ARRAY(DataTypes.STRING)
  }, {});
  Education.associate = function(models) {
    // associations can be defined here
  };
  return Education;
};